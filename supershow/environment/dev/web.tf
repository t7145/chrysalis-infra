resource "random_id" "instance_id" {
  byte_length = 4
}

resource "google_compute_instance" "web_private_1" {
  name         = "${var.app_name}-vm1-${random_id.instance_id.hex}"
  machine_type = "f1-micro"
  zone         = var.zone
  hostname     = "${var.app_name}vm1-${random_id.instance_id.hex}.${var.app_domain}"
  tags         = ["app", "dev", "ssh", "http"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  provisioner "file" {
    source      = "files/install.sh"
    destination = "/tmp"
  }

  provisioner "file" {
    source      = "files/setup.sh"
    destination = "/tmp"
  }

  metadata_startup_script = file("files/provision.sh")

  network_interface {
    network    = google_compute_network.vpc.name
    subnetwork = google_compute_subnetwork.private-subnet.name
  }
}

resource "google_compute_instance" "web_private_2" {
  name         = "${var.app_name}-vm2-${random_id.instance_id.hex}"
  machine_type = "f1-micro"
  zone         = var.zone
  hostname     = "${var.app_name}vm2-${random_id.instance_id.hex}.${var.app_domain}"
  tags         = ["app", "dev", "ssh", "http"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  metadata_startup_script = file("files/provision.sh")

  network_interface {
    network    = google_compute_network.vpc.name
    subnetwork = google_compute_subnetwork.private-subnet.name
  }
}
