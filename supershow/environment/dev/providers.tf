terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }

  backend "gcs" {
    bucket      = "tf-state-dosgames-dev"
    prefix      = "terraform/state"
    credentials = "dosgames-37048845fa42.json"
  }

}

provider "google" {
  credentials = var.credentials_file

  project = var.project
  region  = var.region
  zone    = var.zone
}

provider "google-beta" {
  credentials = var.credentials_file

  project = var.project
  region  = var.region
  zone    = var.zone
}
